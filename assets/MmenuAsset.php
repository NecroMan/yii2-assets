<?php
namespace milano\assets;

class MmenuAsset extends BaseAsset
{

    public $sourcePath = '@bower/jquery.mmenu/dist';
    public $js = [
        'jquery.mmenu.js'
    ];
    public $css = [
        'jquery.mmenu.css'
    ];
    public $depends = [
        'yii\web\JqueryAsset'
    ];

}
