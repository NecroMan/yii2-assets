<?php
namespace milano\assets;

class BLazyAsset extends BaseAsset
{

    public $sourcePath = '@bower/blazy';
    public $js = [
        'blazy.min.js'
    ];
    public $publishOptions = [
        'except' => [
            'example/*',
        ]
    ];
    public $depends = [
        'yii\web\JqueryAsset'
    ];

}
