<?php
namespace milano\assets;

class SidrLightAsset extends BaseAsset
{

    public $sourcePath = '@bower/sidr/dist';
    public $js = [
        'jquery.sidr.min.js'
    ];
    public $css = [
        'stylesheets/jquery.sidr.light.min.css'
    ];
    public $depends = [
        'yii\web\JqueryAsset'
    ];

}
