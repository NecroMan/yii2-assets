<?php
namespace milano\widgets;

use milano\assets\BLazyAsset;
use yii\base\Widget;
use yii\helpers\Json;

class BLazyWidget extends Widget
{

    /** @var bool Enable initialization of LazyLoad on page ready */
    public $autoInit = true;

    /** @var array Additional plugin options */
    public $pluginSettings = [
        'src' => 'data-original',
        'successClass' => 'fadeIn',
    ];

    /** @var string Target selector to apply LazyLoad plugin */
    public $selector = 'img.lazy';

    public function init()
    {
        BLazyAsset::register($this->getView());

        if (!array_key_exists('selector', $this->pluginSettings)) {
            $this->pluginSettings['selector'] = $this->selector;
        }

        $js = [];

        if ($this->autoInit) {
            $js[] = 'var bLazy = new Blazy(' . Json::encode($this->pluginSettings) . ');';
        }

        $this->getView()->registerJs(implode(' ', $js));
    }

}
