<?php
namespace milano\assets;

class SidrDarkAsset extends BaseAsset
{

    public $sourcePath = '@bower/sidr/dist';
    public $js = [
        'jquery.sidr.min.js'
    ];
    public $css = [
        'stylesheets/jquery.sidr.dark.min.css'
    ];
    public $depends = [
        'yii\web\JqueryAsset'
    ];

}
