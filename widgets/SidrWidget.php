<?php
namespace milano\widgets;

use milano\assets\SidrDarkAsset;
use milano\assets\SidrLightAsset;
use yii\base\Widget;
use yii\helpers\Json;

class SidrWidget extends Widget
{

    /** Themes */
    const THEME_DARK = 'dark';
    const THEME_LIGHT = 'light';

    /** @var bool Enable initialization of Sidr on page ready */
    public $autoInit = true;

    /** @var array Additional plugin options */
    public $pluginOptions = [];

    /** @var string Target button selector to toggle Sidr show */
    public $toggleButtonSelector = '.navbar-toggle';

    /** @var string Target selector to transform to Sidr */
    public $menuContainerSelector = '.mobile-menu';

    /** @var string Which theme from defaults should be registered */
    public $theme = self::THEME_DARK;

    public function init()
    {
        if ($this->theme === self::THEME_LIGHT) {
            SidrLightAsset::register($this->getView());
        } else {
            SidrDarkAsset::register($this->getView());
        }

        $js = [];

        if ($this->autoInit) {
            $button = Json::encode($this->toggleButtonSelector);

            if (!array_key_exists('source', $this->pluginOptions)) {
                $this->pluginOptions['source'] = $this->menuContainerSelector;
            }

            $options = Json::encode($this->pluginOptions);

            $js[] = '$(' . $button . ').sidr(' . $options . ');';
        }

        $this->getView()->registerJs(implode(' ', $js));
    }

}
